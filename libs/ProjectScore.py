import requests, json

class ProjectScore:
    score = 0
    test_user_name = "kyeongrok3"
    test_password = "11223344"
    mode = "info"

    def __init__(self, test_user_name):
        self.test_user_name = test_user_name

    def doScore(self, name, url):
        try:
            # self.hello(url, name)
            self.join(url)
            jwt = self.login(url)
            post_id = self.write_a_post(url, jwt)["result"]["postId"]
            self.get_a_post_detail(url, jwt, post_id)
            self.modify_a_post(url,jwt,post_id)
            self.delete_a_post(url, jwt, post_id)
            self.get_posts(url, jwt)
            post_id = self.write_a_post(url, jwt)["result"]["postId"]
            comment_id = self.write_a_comment(url, jwt, post_id)["result"]["id"]
            self.modify_a_comment(url, jwt, post_id, comment_id)
            self.delete_a_comment(url, jwt, post_id, comment_id)
            self.likes(url, jwt, post_id)
            # self.get_my(url, jwt)
            # self.get_likes(url, jwt, post_id)
            # self.alarm(url, jwt, post_id)

        except requests.exceptions.Timeout:
            print(name, "Timeout error:")
        except Exception as e:
            print(name, f"error:{e}", url)

        # try밖에 뺌
        print(name, "score:", self.score)
        return self.score


    def join(self, url):
        # 회원가입
        r = requests.post(f"{url}/api/v1/users/join",
                              json={"userName":self.test_user_name, "password":self.test_password},
                              timeout=2
                              )

        print(r, r.text)

        # 로그인
        j = r.json()
        if(j['resultCode'] == "SUCCESS" and
                j['result']['userName'] == self.test_user_name):
            self.score += 1


    def hello(self, url, keyword='hello'):

        # 배포 되었는지 여부
        url = f"{url}/api/v1/hello"
        #print(url)
        r = requests.get(url, timeout=1)
        print(r, r.text)

        if r.status_code == 200 and r.text == keyword:
            self.score += 1

        return self.score == 1

    def sumOfDigit(self, url, num=9675):

        url = f"{url}/api/v1/hello/{num}"
        r = requests.get(url, timeout=1)

        print(r, r.text)

        if r.status_code == 200 and r.text == "27":
            self.score += 1

        return self.score

    def login(self, url):
        r = requests.post(f"{url}/api/v1/users/login",
                          json={"userName":self.test_user_name, "password":self.test_password},
                          timeout=2
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['jwt'] != None:
            self.score += 1
        return j['result']['jwt']

    def write_a_post(self, url, jwt,title="hello-title",body="hello-body"):
        # 받은 token으로 글쓰기 요청
        # 글 읽기 요청 해서 쓴 글이 잘 등록 되었는지 확인하기
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.post(f"{url}/api/v1/posts",
                          headers = headers,
                          json={"title":title,
                                "body":body},
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' :
            self.score += 1
        return j 

    def get_a_post_detail(self, url, jwt, post_id) :
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.get(f"{url}/api/v1/posts/{post_id}",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['id'] == post_id:
            self.score += 1

    def modify_a_post(self, url, jwt, post_id, title="hello-new-title", body="hello-new-body") :
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.put(f"{url}/api/v1/posts/{post_id}",
                          headers = headers,
                          json={"title" : title,"body":body},
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['postId'] == post_id:
            self.score += 1

    def get_posts(self, url, jwt) :
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.get(f"{url}/api/v1/posts",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r, json.dumps(j))
        if j['resultCode'] == 'SUCCESS' and j['result']['pageable'] != None:
            self.score += 1
    
    def delete_a_post(self, url, jwt, post_id) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.delete(f"{url}/api/v1/posts/{post_id}",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['postId']== post_id :
            self.score += 1

    def get_comments(self, url, jwt, post_id) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.get(f"{url}/api/v1/posts/{post_id}/comments",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['pageable'] != None and j['result']['pageable']['pageSize'] == 10:
            self.score += 1

    def write_a_comment(self, url, jwt, post_id, comment="hello-comment") : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.post(f"{url}/api/v1/posts/{post_id}/comments",
                          headers = headers,
                          json={"comment" : comment},
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['comment']== comment:
            self.score += 1
        return j

    def modify_a_comment(self, url, jwt, post_id, comment_id, comment = "modified-comment") :
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.put(f"{url}/api/v1/posts/{post_id}/comments/{comment_id}",
                          headers = headers,
                          json={"comment" : comment},
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['id'] == comment_id and j['result']['comment'] == comment :
            self.score += 1


    def delete_a_comment(self, url, jwt, post_id, comment_id) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.delete(f"{url}/api/v1/posts/{post_id}/comments/{comment_id}",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r, j)
        if j['resultCode'] == 'SUCCESS' and j['result']['id']== comment_id :
            self.score += 1

    def get_my(self, url, jwt) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.get(f"{url}/api/v1/posts/my",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r,j)
        if j['resultCode'] == 'SUCCESS' and j['result']['pageable'] != None and j['result']['pageable']['pageSize'] == 20:
            self.score += 1    
    
    def get_likes(self, url, jwt, post_id) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.get(f"{url}/api/v1/posts/{post_id}/likes",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r,j)
        if j['resultCode'] == 'SUCCESS' and j['result'] == 1:
            self.score += 1                

    def likes(self, url,jwt, post_id) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.post(f"{url}/api/v1/posts/{post_id}/likes",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r,j) 
        if j['resultCode'] == 'SUCCESS':
            self.score += 1       


    def alarm(self, url, jwt, post_id) : 
        headers = {"Content-Type": "application/json; charset=utf-8",
                   "Authorization":f"Bearer {jwt}"}
        r = requests.get(f"{url}/api/v1/users/alarm",
                          headers = headers,
                          timeout=3
                          )
        j = r.json()
        print(r,j) 

        score = True

        for alarm in j['result']['content'] :
            if alarm['alarmArgs']['targetId'] != post_id : 
                False
                break

        if j['resultCode'] == 'SUCCESS' and score:
            self.score += 1       
