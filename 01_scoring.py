import requests
from libs.GoogleSpreadsheetUtil import GoogleSpreadsheet
from libs.ProjectScore import ProjectScore

if __name__ == '__main__':
    sheet_url = "https://docs.google.com/spreadsheets/d/1cJ9XQDISo3B6UHzcDmWtG9ucDRDg_YgJPkkW9atCFjk"
    sheet_name = "종합프로젝트"
    test_user_name = "kyeongrok43"
    gs = GoogleSpreadsheet(sheet_url, sheet_name)
    gs.update_student_list('A4:C93')
    infos = gs.read_saved_csv()
    scores_ = []
    test_passed_students = []

    for i, info in list(enumerate(infos)):
        score = 0

        try:
            if info[2] != "\n":
                url = info[2]
                name = info[1]
                ps = ProjectScore(test_user_name)
                score = ps.doScore(name, url)
                if score >= 6:
                    test_passed_students.append(name)

            scores_.append([name, score])

        except Exception as e:
            print(e)

    print(scores_)

    gs.write_score(sheet_name, "D3:D93", scores_)
    print(len(test_passed_students), test_passed_students)
