import requests
from libs.ProjectScore import ProjectScore

# 혼자 채점용
# 사용방법 아래 url, name에 본인의 api url과 이름을 넣고 실행 하세요.
if __name__ == '__main__':
    test_user_name = "kyeongrok35"

    url = "http://ec2-3-38-173-194.ap-northeast-2.compute.amazonaws.com:8081/swagger-ui/"
    name = "eee"
    try:
        ps = ProjectScore(test_user_name)
        ps.doScore(name, url)

    except requests.exceptions.Timeout:
        print(name, "Timeout error:")
    except Exception as e:
        print(name, f"error:{e}", url)



